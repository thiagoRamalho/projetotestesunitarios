package br.com.unitarios.teste;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.unitarios.negocio.GerenciadorDados;
import br.com.unitarios.negocio.RecuperacaoDados;
import br.com.unitarios.negocio.RecuperacaoDadosPorArquivo;
import br.com.unitarios.negocio.StatusOperacao;

public class GerenciadorDadosComParcialMockTest {

	//atributo utilit�rio apenas para melhorar a visualiza��o dos testes
	private static int count = 0;
	
	
/*	 A anota��o @Test identica para o framework JUnit que 
	 o m�todo � um cen�rio de teste 
	
	 Spy � um recurso da biblioteca do mockito para um conceito conhecido
	 como partialMock, nesse conceito n�s n�o mockamos uma classe completamente
	 apenas algum(ns) m�todos para satisfazer a necessidade dos nossos testes
*/	
	
	//classe que ser� testada
	private GerenciadorDados gerenciadorDados;

	/**
	 * M�todo setUp por possuir a anota��o @Before sempre
	 * ser� executado antes de cada teste
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		System.out.println("----- Spy "+(++count)+" -----");
		
		//obtemos uma inst�ncia nova antes de cada teste
		gerenciadorDados = new GerenciadorDados();
	}
	
	/**
	 * M�todo tearDown por possuir a anota��o @After sempre
	 * ser� executado ap�s cada teste
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown(){
		//apenas para fins did�ticos sempre imprimimos a 
		//mensagem ap�s os testes
		System.out.println(gerenciadorDados.getMensagem());
	}

	/**
	 * Nesse teste o valor final para o dado deve ser 1 pois
	 * esperamos que a leitura do arquivo retorne zero e a regra
	 * de ajuste execute corretamente
	 */
	@Test
	public void deveAplicarAjusteDe1PoisDadoEstaZerado(){
		
		//queremos testar o m�todo aplicarAjuste(), por�m n�o queremos acessar
		//um arquivo real para obter dados, ent�o iremos mockar parcialmente
		//o recuperador de dados por arquivo, assim o restante dos m�todos
		//da classe ser�o executados exceto o acesso ao arquivo
		
		//criamos objeto real por�m "vigiado" pela biblioteca de mock
		RecuperacaoDados recuperacaoDados = spy(new RecuperacaoDadosPorArquivo());
		
		//definimos que a chamada ao m�todo obterInformacoes() � simulado, for�ando
		//o resultado de sucesso
		doReturn(StatusOperacao.SUCESSO).when(recuperacaoDados).obterInformacoes();
			
		//para confirmar que o restante dos m�todos est� sendo executados, iremos
		//invocar a chamada ao getDados() (deve retornar zero)
		assertEquals(0, recuperacaoDados.getDados().intValue());
		
		//executamos normalmente a classe em teste...
		StatusOperacao status = gerenciadorDados.executar(recuperacaoDados);
		
		//m�todos asserts confrontam o resultado esperado com o resultado obtido 
		//(esquerda mensagem explicativa (opcional), centro esperado, direita obtido)
		assertEquals("Deve retornar SUCESSO", StatusOperacao.SUCESSO, status);
		
		//como ESPERAMOS o retorno SUCESSO n�o DEVE SER criada qualquer mensagem de erro
		//para confirmar tamb�m criamos um assert confrontando esse comportamento
		//note que n�o utilizamos a mensagem opcional explicativa nesse caso
		assertEquals(null, gerenciadorDados.getMensagem());
		
		//confrontamos a resposta do valor do dado
		assertEquals(1, gerenciadorDados.getDado().intValue());
		
		System.out.println("Resultado: "+(gerenciadorDados.getDado().intValue()));
	}
	
	/**
	 * Nesse teste o valor final para o dado deve ser 1 pois
	 * esperamos que a leitura do arquivo retorne zero e a regra
	 * de ajuste execute corretamente
	 */
	@Test
	public void deveAplicarAjusteDobrandoValorDoDado(){
		
		//queremos testar o m�todo aplicarAjuste(), por�m n�o queremos acessar
		//um arquivo real para obter dados, ent�o iremos mockar parcialmente
		//o recuperador de dados por arquivo, assim o restante dos m�todos
		//da classe ser�o executados exceto o acesso ao arquivo
		
		//criamos objeto real por�m "vigiado" pela biblioteca de mock
		RecuperacaoDados recuperacaoDados = spy(new RecuperacaoDadosPorArquivo());
		
		//definimos que a chamada ao m�todo obterInformacoes() � simulado, for�ando
		//o resultado de sucesso
		doReturn(StatusOperacao.SUCESSO).when(recuperacaoDados).obterInformacoes();
		
		//vamos for�ar tamb�m o retorno de obter dados
		doReturn(new BigDecimal("100")).when(recuperacaoDados).getDados();
		
		//executamos normalmente a classe em teste...
		StatusOperacao status = gerenciadorDados.executar(recuperacaoDados);
		
		//m�todos asserts confrontam o resultado esperado com o resultado obtido 
		//(esquerda mensagem explicativa (opcional), centro esperado, direita obtido)
		assertEquals("Deve retornar SUCESSO", StatusOperacao.SUCESSO, status);
		
		//como ESPERAMOS o retorno SUCESSO n�o DEVE SER criada qualquer mensagem de erro,
		//para confirmar tamb�m criamos um assert confrontando esse comportamento
		//note que n�o utilizamos a mensagem opcional explicativa nesse caso
		assertEquals(null, gerenciadorDados.getMensagem());
		
		//confrontamos a resposta o m�todo aplicar ajuste deve dobrar o valor
		//recebido do recuperador (for�ado como 100)
		assertEquals(200, gerenciadorDados.getDado().intValue());
		
		System.out.println("Resultado: "+(gerenciadorDados.getDado().intValue()));
	}

}

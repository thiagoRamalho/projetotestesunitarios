package br.com.unitarios.teste;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.unitarios.negocio.GerenciadorDados;
import br.com.unitarios.negocio.RecuperacaoDados;
import br.com.unitarios.negocio.StatusOperacao;
import br.com.unitarios.negocio.TipoRecuperacao;
//para utilizar o mockito ser� necess�rio import statico

public class GerenciadorDadosComMockTest {

	//atributo utilit�rio apenas para melhorar a visualiza��o dos testes
	private static int count = 0;
	
/*	  A anota��o @Test identica para o framework JUnit que 
	  o m�todo � um cen�rio de teste 
*/	
	
	//classe que ser� testada
	private GerenciadorDados gerenciadorDados;

	/**
	 * M�todo setUp por possuir a anota��o @Before sempre
	 * ser� executado antes de cada teste
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		System.out.println("----- Mock "+(++count)+" -----");
		
		//obtemos uma inst�ncia nova antes de cada teste
		gerenciadorDados = new GerenciadorDados();
	}
	
	/**
	 * M�todo tearDown por possuir a anota��o @After sempre
	 * ser� executado ap�s cada teste
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown(){
		//apenas para fins did�ticos sempre imprimimos a 
		//mensagem ap�s os testes
		System.out.println(gerenciadorDados.getMensagem());
	}

	/**
	 * Nesse cen�rio a resposta a execu��o deve ser SUCESSO
	 * recuperador de dados n�o encontrou erros
	 */
	@Test
	public void deveExecutarProcessoSemErros(){
		
		//ainda n�o possuimos ou n�o queremos utilizar uma
		//implementa��o real de RecuperacaoDados, ent�o 
		//mockamos a inteface pois nosso objetivo � testar
		//a classe gerenciadorDados
		
		//utilizando a biblioteca de mocks para simular uma
		//implementa��o da interface
		RecuperacaoDados recuperacaoDados = mock(RecuperacaoDados.class);
		
		//agora podemos simular as respostas dos m�todos do objeto mockado
		//nesse exemplo simularemos um retorno ok
		when(recuperacaoDados.obterInformacoes()).thenReturn(StatusOperacao.SUCESSO);
		//nesse exemplo simulamos a recupera��o por banco de dados
		when(recuperacaoDados.getTipo()).thenReturn(TipoRecuperacao.PERSISTENCIA);
		
		//executamos normalmente a classe em teste...
		StatusOperacao status = gerenciadorDados.executar(recuperacaoDados);
		
		//m�todos asserts confrontam o resultado esperado com o resultado obtido 
		//(esquerda mensagem explicativa (opcional), centro esperado, direita obtido)
		assertEquals("Deve retornar SUCESSO", StatusOperacao.SUCESSO, status);
		
		//como simulamos o retorno SUCESSO n�o DEVE SER criada qualquer mensagem de erro
		//para confirmar tamb�m criamos um assert confrontando esse comportamento
		//note que n�o utilizamos a mensagem opcional explicativa nesse caso
		assertEquals(null, gerenciadorDados.getMensagem());
	}
	
	/**
	 * Nesse cen�rio o teste deve obter resposta negativa da classe
	 * gerenciadora, pois o recuperador de dados respondeu com erro
	 * a chamada ao arquivo
	 */
	@Test
	public void deveGerarErroAoAcessarArquivo(){
		
		//ainda n�o possuimos ou n�o queremos utilizar uma
		//implementa��o real de RecuperacaoDados, ent�o 
		//mockamos a inteface pois nosso objetivo � testar
		//a classe gerenciadorDados
		
		//utilizando a biblioteca de mocks para simular uma
		//implementa��o da interface
		RecuperacaoDados recuperacaoDados = mock(RecuperacaoDados.class);
		
		//agora podemos simular as respostas dos m�todos do objeto mockado
		//simulando um erro ao tentar obter dados
		when(recuperacaoDados.obterInformacoes()).thenReturn(StatusOperacao.ERRO);
		//nesse exemplo simulamos a recupera��o por arquivo
		when(recuperacaoDados.getTipo()).thenReturn(TipoRecuperacao.ARQUIVO);
		
		//executamos normalmente a classe em teste...
		StatusOperacao status = gerenciadorDados.executar(recuperacaoDados);
		
		//m�todos asserts confrontam o resultado esperado com o resultado obtido 
		//(esquerda mensagem explicativa (opcional), centro esperado, direita obtido)
		assertEquals("erro ao acessar arquivo", StatusOperacao.ERRO, status);
		
		//como simulamos o retorno ERRO DEVE SER retornada mensagem de erro
		//para acesso ao recuperador por arquivo
		
		//mensagem esperada
		String mensagemErro = GerenciadorDados.ERRO_OPERACAO_NAO_EXECUTADA+GerenciadorDados.ERRO_ARQUIVO;
		
		//para confirmar tamb�m criamos um assert confrontando esse comportamento
		//note que n�o utilizamos a mensagem opcional explicativa nesse caso
		assertEquals(mensagemErro, gerenciadorDados.getMensagem());
	}
	
	/**
	 * Nesse cen�rio o teste deve obter resposta negativa da classe
	 * gerenciadora, pois o recuperador de dados respondeu com erro
	 * a chamada ao banco
	 */
	@Test
	public void deveGerarErroAoAcessarBancoDeDados(){
		
		//ainda n�o possuimos ou n�o queremos utilizar uma
		//implementa��o real de RecuperacaoDados, ent�o 
		//mockamos a inteface pois nosso objetivo � testar
		//a classe gerenciadorDados
		
		//utilizando a biblioteca de mocks para simular uma
		//implementa��o da interface
		RecuperacaoDados recuperacaoDados = mock(RecuperacaoDados.class);
		
		//agora podemos simular as respostas dos m�todos do objeto mockado
		//simulando um erro ao tentar obter dados
		when(recuperacaoDados.obterInformacoes()).thenReturn(StatusOperacao.ERRO);
		//nesse exemplo simulamos a recupera��o por arquivo
		when(recuperacaoDados.getTipo()).thenReturn(TipoRecuperacao.PERSISTENCIA);
		
		//executamos normalmente a classe em teste...
		StatusOperacao status = gerenciadorDados.executar(recuperacaoDados);
		
		//m�todos asserts confrontam o resultado esperado com o resultado obtido 
		//(esquerda mensagem explicativa (opcional), centro esperado, direita obtido)
		assertEquals("erro ao acessar banco", StatusOperacao.ERRO, status);
		
		//como simulamos o retorno ERRO DEVE SER retornada mensagem de erro
		//para acesso ao recuperador por arquivo
		
		//mensagem esperada
		String mensagemErro = GerenciadorDados.ERRO_OPERACAO_NAO_EXECUTADA+GerenciadorDados.ERRO_PERSISTENCIA;
		
		//para confirmar tamb�m criamos um assert confrontando esse comportamento
		//note que n�o utilizamos a mensagem opcional explicativa nesse caso
		assertEquals(mensagemErro, gerenciadorDados.getMensagem());
	}

	//TODO: DEMONSTRAR WEBSERVICE
}

/**
 * 
 */
package br.com.unitarios.teste;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import br.com.unitarios.negocio.IEstrategia;
import br.com.unitarios.negocio.Negocio;
import br.com.unitarios.negocio.TipoOperacao;

/**
 * @author thiago.ramalho
 *
 */
public class NegocioMockTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void deveRetornar2() {
		
		IEstrategia estrategia = mock(IEstrategia.class);

		when(estrategia.getTipo()).thenReturn(TipoOperacao.EXECUTACAO_C);

		Negocio negocio = new Negocio(estrategia);
		
		assertEquals(2, negocio.executar());
	}


	@Test
	public void deveRetornar1() {
		
		IEstrategia estrategia = mock(IEstrategia.class);

		when(estrategia.getTipo()).thenReturn(TipoOperacao.EXECUTACAO_B);

		Negocio negocio = new Negocio(estrategia);
		
		assertEquals(1, negocio.executar());
	}
	
	@Test
	public void deveRetornarZero() {
		
		IEstrategia estrategia = mock(IEstrategia.class);

		//when(estrategia.getTipo()).thenReturn(TipoOperacao.EXECUTACAO_B);

		Negocio negocio = new Negocio(estrategia);
		
		assertEquals(0, negocio.executar());
	}
}

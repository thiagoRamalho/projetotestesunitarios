/**
 * 
 */
package br.com.unitarios.teste;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import br.com.unitarios.negocio.Calculadora;

/**
 * @author thiago.ramalho
 *
 */
public class CalculadoraTest {

	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Deve dividir - 12 por 3 - Resultado 4
	 */
	@Test
	public void dividir12Por3Test() {
		long dividendo = 12;
		long divisor   = 3;
		
		double actual = new Calculadora().dividir(dividendo, divisor);
		
		assertEquals(new BigDecimal("4"), new BigDecimal(actual));
	}
	
	/**
	 * Deve dividir - 12 por 12 - Resultado 1
	 */
	@Test
	public void dividir12Por12Test() {
		
		long dividendo = 12;
		long divisor   = 12;
		
		double actual = new Calculadora().dividir(dividendo, divisor);
		
		assertEquals(new BigDecimal("1"), new BigDecimal(actual));
	}
	
/*	*//**
	 * Deve tentar divis�o e gerar exception
	 *//*
	@Test(expected = ArithmeticException.class)
	public void dividir3Por0Test() {
		
		long dividendo = 3;
		long divisor   = 0;
		
		new Calculadora().dividir(dividendo, divisor);
	}*/

}

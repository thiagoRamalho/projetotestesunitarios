package br.com.unitarios.teste.solid;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.unitarios.negocio.solid.Entidade;
import br.com.unitarios.negocio.solid.ProcessadorOriginal;

public class ProcessadorTest {

	private ProcessadorOriginal processador;


	@Before
	public void setUp() throws Exception {
		processador = new ProcessadorOriginal();
	}

	/**
	 * Deve processar os dados extraindo valor com no maximo
	 * 10 caracteres e versao com apenas 1 caracter
	 * nao deve extrair data
	 * 
	 * Entrada:   1 DADOS COM 20 ELEMN 20131231
	 * VERSAO | VALOR | DATA
	 * Resultado:[1][ DADOS COM][]
	 */
	@Test
	public void deve_processar_versao_1(){
		
		String dados = "1 DADOS COM 20 ELEMN 20131231";
		
		Entidade entidade = this.processador.executar(dados);
		
		assertEquals(1, entidade.getVersao());
		assertEquals(" DADOS COM", entidade.getValor());
		assertEquals("", entidade.getData());
		
	}
	
	/**
	 * Deve processar os dados extraindo valor com no maximo
	 * 20 caracteres e versao com apenas 1 caracter
	 * deve extrair data com 8 caracteres
	 * 
	 * Entrada:   1 DADOS COM 20 ELEMN 20131231
	 * VERSAO | VALOR | DATA
	 * Resultado:[1][ DADOS COM 20 ELEMN ][20131231]
	 */
	@Test
	public void deve_processar_versao_2(){
		
		String dados = "2 DADOS COM 20 ELEMN 20131231";
		
		Entidade entidade = this.processador.executar(dados);
		
		assertEquals(2, entidade.getVersao());
		assertEquals(" DADOS COM 20 ELEMN ", entidade.getValor());
		assertEquals("20131231", entidade.getData());
		
	}

	/**
	 * Deve processar os dados extraindo valor com no maximo
	 * 20 caracteres e versao com apenas 1 caracter
	 * deve extrair data com 8 caracteres
	 * 
	 * Entrada:   1 DADOS COM 20 ELEMN 20131231
	 * VERSAO | VALOR | DATA
	 * Resultado:[1][ DADOS COM 20 ELEMN ][131231]
	 */
/*	@Test
	public void deve_processar_versao_3(){
		
		String dados = "3 DADOS COM 20 ELEMN 20131231";
		
		Entidade entidade = this.processador.executar(dados);
		
		assertEquals(3, entidade.getVersao());
		assertEquals(" DADOS COM 20 ELEMN ", entidade.getValor());
		assertEquals("131231", entidade.getData());
		
	}
*/
}

package br.com.unitarios.negocio;

import java.math.BigDecimal;



/**
 * 
 * @author       Thiago
 * @Date         30/03/2013
 * @project      ProjetoTestesUnitarios
 * @Description: Classe respons�vel por controlar o acesso a informa��o
 */
public class GerenciadorDados {

	public static final String ERRO_OPERACAO_NAO_EXECUTADA = "Erro: opera��o n�o executada, ";
	public static final String ERRO_ARQUIVO      = "n�o foi poss�vel acessar o arquivo";
	public static final String ERRO_PERSISTENCIA = "n�o foi poss�vel acessar o banco de dados";
	public static final String ERRO_WEB_SERVICE  = "n�o foi possivel acessar o servidor";
	
	private String mensagem;
	private BigDecimal dado;
	
	public StatusOperacao executar(RecuperacaoDados recuperacaoDados){

		StatusOperacao statusOperacao   = StatusOperacao.ERRO;

		TipoRecuperacao tipoRecuperacao = recuperacaoDados.getTipo();

		//tenta obter as informa��es da implementa��o enviada
		statusOperacao = recuperacaoDados.obterInformacoes();

		//verifica se opera��o foi executada corretamente e 
		//formata mensagem de erro caso contr�rio
		verificarStatus(statusOperacao, tipoRecuperacao);

		//se foi poss�vel obter a informa��o...
		if(StatusOperacao.SUCESSO.equals(statusOperacao)){
			//executa l�gica com os dados
			dado = recuperacaoDados.getDados();

			if(TipoRecuperacao.ARQUIVO.equals(tipoRecuperacao)){
				this.aplicarAjuste();
			}
		}

		return statusOperacao;
	}

	/**
	 * Aplica ajuste vari�vel com base no valor
	 * j� existente
	 */
	private void aplicarAjuste() {

		//se o dado veio zerado ajustamos para 1
		if(this.dado.compareTo(BigDecimal.ZERO) == 0){

			this.dado = this.dado.add(BigDecimal.ONE);
		}
		else {
			//do contr�rio dobramos o seu valor
			this.dado = this.dado.multiply(new BigDecimal("2"));
		}
	}

	/**
	 * Formata a mensagem de erro caso o status n�o seja SUCESSO
	 * @param statusOperacao
	 */
	private void verificarStatus(final StatusOperacao statusOperacao, final TipoRecuperacao tipoRecuperacao) {

		if(StatusOperacao.ERRO.equals(statusOperacao)){
			this.mensagem = ERRO_OPERACAO_NAO_EXECUTADA;

			if(TipoRecuperacao.ARQUIVO.equals(tipoRecuperacao)){
				this.mensagem += ERRO_ARQUIVO;
			}
			else if(TipoRecuperacao.PERSISTENCIA.equals(tipoRecuperacao)){
				this.mensagem += ERRO_PERSISTENCIA;
			}
			else if(TipoRecuperacao.WEB_SERVICE.equals(tipoRecuperacao)){
				this.mensagem += ERRO_WEB_SERVICE;
			}
		}
	}
	
	public BigDecimal getDado(){
		return this.dado;
	}
	
	public String getMensagem(){
		return this.mensagem;
	}
}

package br.com.unitarios.negocio.solid;

import br.com.unitarios.negocio.solid.Entidade;

public class ConversorVersao2 implements Conversor {

	
	public Entidade executar(String dados){
		
		int versao = new Integer(dados.substring(0, 1)).intValue();
		String valor = dados.substring(1,21);
		String data  = dados.substring(21,29);
		
		Entidade e = new Entidade(versao, valor, data);
		
		return e;
	}
}

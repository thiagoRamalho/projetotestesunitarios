package br.com.unitarios.negocio.solid;


public class ProcessadorSRP {


	public Entidade executar(String dados){

		Entidade entidade = this.converter(dados);


		return entidade;

	}

	private Entidade converter(String dados) {

		int versao = new Integer(dados.substring(0, 1)).intValue();

		Conversor conversor = null;
		
		if(versao == 1){
			conversor = new ConversorVersao1();
		}
		else if(versao == 2){
			conversor = new ConversorVersao2();
		}
		else if(versao == 3){
			conversor = new ConversorVersao3();
		}

		Entidade entidade = conversor.executar(dados);

		return entidade;
	}
}

package br.com.unitarios.negocio.solid;


public class ConversorVersao1 implements Conversor {

	
	public Entidade executar(String dados){
		
		int versao = new Integer(dados.substring(0, 1)).intValue();
		String valor = dados.substring(1,11);
		String data  = "";
		
		Entidade e = new Entidade(versao, valor, data);
		
		return e;
	}
}

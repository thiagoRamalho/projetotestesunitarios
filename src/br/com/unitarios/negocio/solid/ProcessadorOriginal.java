package br.com.unitarios.negocio.solid;


public class ProcessadorOriginal {


	public Entidade executar(String dados){

		Entidade entidade = this.converter(dados);


		return entidade;

	}

	private Entidade converter(String dados) {

		int versao = new Integer(dados.substring(0, 1)).intValue();
		String valor = "";
		String data  = "";


		if(versao == 1){
			valor = dados.substring(1,11);
		}
		else {
			valor = dados.substring(1,21);
			data  = dados.substring(21,29);
			
/*			if(versao == 3){
				
				data = data.substring(2);
			}
*/		}

		Entidade entidade = new Entidade(versao, valor, data);

		return entidade;
	}
}

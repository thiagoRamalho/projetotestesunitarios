package br.com.unitarios.negocio.solid;

public class Entidade {
	
	private int versao;
	private String valor;
	private String data;

	public Entidade(int versao, String valor, String data) {
		super();
		this.versao = versao;
		this.valor = valor;
		this.data = data;
	}

	public int getVersao() {
		return versao;
	}

	public String getValor() {
		return valor;
	}
	
	public String getData(){
		return data;
	}
	
	public void setData(String data){
		this.data = data;
	}
	
}

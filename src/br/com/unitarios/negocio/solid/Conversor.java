package br.com.unitarios.negocio.solid;

public interface Conversor {

	public Entidade executar(String dados);
}
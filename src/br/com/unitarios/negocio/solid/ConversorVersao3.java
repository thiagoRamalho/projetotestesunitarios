package br.com.unitarios.negocio.solid;

public class ConversorVersao3 extends ConversorVersao2 {

	@Override
	public Entidade executar(String dados) {
		
		Entidade e = super.executar(dados);
		
		String data = e.getData();
		
		e.setData(data.substring(2));
		
		return e;
	}

}

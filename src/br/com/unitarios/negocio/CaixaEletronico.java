package br.com.unitarios.negocio;


public class CaixaEletronico
{
	private IContaCorrente _contaCorrente;

	public CaixaEletronico(IContaCorrente contaCorrente)
	{
		this._contaCorrente = contaCorrente;
	}

	public void depositar(double valor)
	{
		this._contaCorrente.setSaldo(valor+this._contaCorrente.getSaldo());
		this._contaCorrente.depositar(valor);
	}

	public void Sacar(double valor)
	{
		this._contaCorrente.sacar(valor);
	}

	public double ExibirSaldo()
	{
		return this._contaCorrente.getSaldo();
	}
}

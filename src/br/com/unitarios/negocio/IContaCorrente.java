/**
 * 
 */
package br.com.unitarios.negocio;

/**
 * @author thiago.ramalho
 *
 */
public interface IContaCorrente {
	
        public abstract double getSaldo();

        public abstract double setSaldo(double valor);
        
        public abstract void depositar(double valorDeposito);
        
        public abstract void sacar(double valorSaque);
}

/**
 * 
 */
package br.com.unitarios.negocio;

/**
 * @author thiago.ramalho
 *
 */
public class Negocio {

	private IEstrategia estrategia;
	
	/**
	 * 
	 */
	public Negocio(IEstrategia estrategia) {
		this.estrategia = estrategia;
	}


	public int executar(){

		int valor = 0;

		if(TipoOperacao.EXECUTACAO_B.equals(estrategia.getTipo())){
			valor = 1;

		} else if(TipoOperacao.EXECUTACAO_C.equals(estrategia.getTipo())){
			valor = 2;

		}

		return valor;
	}
}

package br.com.unitarios.negocio;

import java.math.BigDecimal;

/**
 * 
 * @author       Thiago
 * @Date         30/03/2013
 * @project      ProjetoTestesUnitarios
 * @Description: Interface com o contrato que deve ser seguido pelas 
 *               implementa�oes que obtem as informa��es
 */
public interface RecuperacaoDados {

	/**
	 * Executa o processo de obte��o de dados e retorna o status
	 * da execu��o
	 * 
	 * @return
	 */
	public StatusOperacao obterInformacoes();

	/**
	 * Retorna o tipo de obten��o de dados com base no
	 * enum #TipoRecuperacao
	 * @return
	 */
	public TipoRecuperacao getTipo();

	/**
	 * Retornamos os dados obtidos pela execu��o do m�todo
	 * {@link #obterInformacoes()}
	 * @return
	 */
	public BigDecimal getDados();
}

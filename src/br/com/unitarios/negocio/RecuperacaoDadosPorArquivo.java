package br.com.unitarios.negocio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * 
 * @author       Thiago
 * @Date         30/03/2013
 * @project      ProjetoTestesUnitarios
 * @Description: Classe que implementa o contrato de obten��o dos dados
 *               pesquisando essa informa��o em arquivo
 */
public class RecuperacaoDadosPorArquivo implements RecuperacaoDados{

	private String caminhoArquivo;
	private BigDecimal dado;

	@Override
	public StatusOperacao obterInformacoes() {		

		StatusOperacao statusOperacao = StatusOperacao.SUCESSO;

		try {
			//tentamos ler o arquivo
			BufferedReader br = new BufferedReader(new FileReader(this.caminhoArquivo));;

			//somente a primeira linha nos interessa
			if(br.ready()){ 
				String linha = br.readLine(); 
				dado = new BigDecimal(linha); 
			}
			
			br.close();

		} catch (FileNotFoundException e) {
			statusOperacao = StatusOperacao.ERRO;

		} catch (IOException e) {
			statusOperacao = StatusOperacao.ERRO;
		} 

		return statusOperacao; 
	}

	@Override
	public TipoRecuperacao getTipo() {
		return TipoRecuperacao.ARQUIVO;
	}

	@Override
	public BigDecimal getDados() {
		if(dado == null){
			dado = new BigDecimal("0");
		}
		return dado;
	}
}

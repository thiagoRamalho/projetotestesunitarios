/**
 * 
 */
package br.com.unitarios.negocio;


public class Calculadora {

	public double dividir(long dividendo, long divisor){
		
		try{
			
			return dividendo / divisor;
			
		}catch (ArithmeticException e) {
			throw new ArithmeticException("O divisor n�o pode ser zero!");
		}
	}
}

/**
 * 
 */
package br.com.unitarios.negocio;

/**
 * @author thiago.ramalho
 *
 */
public interface IEstrategia {

	/**
	 * @return
	 */
	public abstract TipoOperacao getTipo();

}
